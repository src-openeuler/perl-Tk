%define perlver %(eval "`%{__perl} -V:version`"; echo $version)

Name:           perl-Tk
Version:        804.036
Release:        2
Summary:        A graphical user interface toolkit for Perl
License:        (GPL-1.0-or-later OR Artistic-1.0-Perl) and SWL
URL:            https://metacpan.org/release/Tk
Source0:        https://cpan.metacpan.org/authors/id/S/SR/SREZIC/Tk-%{version}.tar.gz
Patch0000:      perl-Tk-widget.patch
Patch0001:      perl-Tk-seg.patch
BuildRequires:  gcc-c++ perl-devel >= 3:5.8.3 perl-generators freetype-devel libjpeg-devel
BuildRequires:  libpng-devel libX11-devel libXft-devel perl(Config) perl(Cwd)
BuildRequires:  perl(File::Copy) perl(lib) perl(open) perl(strict) perl(Test)
BuildRequires:  perl(AutoLoader) perl(ExtUtils::MakeMaker) perl(base) perl(Carp)
BuildRequires:  perl(DirHandle) perl(DynaLoader) perl(Encode) perl(Exporter)
BuildRequires:  perl(File::Basename) perl(File::Spec) perl(if) perl(locale)
BuildRequires:  perl(IO::Handle) perl(overload) perl(subs) perl(Symbol) perl(Text::Tabs)
BuildRequires:  perl(vars) perl(warnings) perl(XSLoader) xorg-x11-server-Xvfb
BuildRequires:  xorg-x11-xinit font(:lang=en) liberation-sans-fonts perl(constant)
BuildRequires:  perl(Data::Dumper) perl(Devel::Peek) perl(ExtUtils::Command::MM)
BuildRequires:  perl(File::Spec::Functions) perl(File::Temp) perl(FindBin) perl(Getopt::Long)
BuildRequires:  perl(IO::Socket) perl(POSIX) perl(Test::More) perl(utf8) perl(MIME::Base64)
Provides:       perl(Tk::LabRadio) = 4.004 perl(Tk) = %{version}

%{?perl_default_filter}
%global __provides_exclude %{?__provides_exclude:%__provides_exclude|}perl\\(Tk\\)
%global __provides_exclude %__provides_exclude|perl\\(Tk::Clipboard\\)$
%global __provides_exclude %__provides_exclude|perl\\(Tk::Frame\\)$
%global __provides_exclude %__provides_exclude|perl\\(Tk::Listbox\\)$
%global __provides_exclude %__provides_exclude|perl\\(Tk::Scale\\)$
%global __provides_exclude %__provides_exclude|perl\\(Tk::Scrollbar\\)$
%global __provides_exclude %__provides_exclude|perl\\(Tk::Table\\)$
%global __provides_exclude %__provides_exclude|perl\\(Tk::Toplevel\\)$
%global __provides_exclude %__provides_exclude|perl\\(Tk::Widget\\)$
%global __provides_exclude %__provides_exclude|perl\\(Tk::Wm\\)$

%description
Perl-Tk is a collection of modules and code that attempts to wed the easily configured
Tk widget toolkit to the powerful lexigraphic, dynamic memory, I/O, and object-oriented
capabilities of Perl 5. In other words, it is an interpreted scripting language for making
widgets and programs with Graphical User Interfaces (GUI).

%package     devel
Summary:     Development files for perl-Tk
Requires:    perl-Tk = %{version}-%{release}

%description devel
This package provides the modules and Tk code for Perl/Tk.

%package_help

%prep
%autosetup -n Tk-%{version} -p1
find . -type f -exec %{__perl} -pi -e \
's,^(#!)(/usr/local)?/bin/perl\b,$1%{__perl}, if ($. == 1)' {} \;
chmod -x pod/Popup.pod Tixish/lib/Tk/balArrow.xbm
%{__perl} -pi -e 's,\@demopath\@,%{_pkgdocdir}/demos,g' demos/widget

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor X11LIB=%{_libdir} XFT=0
find . -name Makefile | xargs %{__perl} -pi -e 's/^\tLD_RUN_PATH=[^\s]+\s*/\t/'
%make_build

%check
xvfb-run -a make test

%install
%make_install

chmod -R u+rwX,go+rX,go-w $RPM_BUILD_ROOT/*
mkdir __demos
cp -pR $RPM_BUILD_ROOT%{perl_vendorarch}/Tk/demos __demos
find __demos/ -type f -exec chmod -x {} \;

rm -f %{buildroot}%{_bindir}/{gedi,widget}
rm -fr %{buildroot}%{perl_vendorarch}/Tk/demos

%files
%license COPYING
%doc Changes README README.linux ToDo pTk/*license* __demos/demos demos/widget
%{_bindir}/{p*,tkjpeg}
%{perl_vendorarch}/auto/Tk
%{perl_vendorarch}/T*

%files devel
%dir %{perl_vendorarch}/Tk
%{perl_vendorarch}/Tk/{MMutil.pm,install.pm,MakeDepend.pm}

%files help
%{_mandir}/man?/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 804.036-2
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 804.036-1
- Upgrade to version 804.036

* Mon Aug 17 2020 lingsheng <lingsheng@huawei.com> - 804.035-1
- Update to 804.035

* Fri Dec 20 2019 shijian <shijian16@huawei.com> - 804.034-5
- Package init
